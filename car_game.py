cmd = ""
started = False
while True:
    cmd = input("> ").lower()
    if cmd == "start":
        if started:
            print("Car is already started...")
        else:
            started = True
            print(" Car started... ready to go!")
            
    elif cmd == "stop":
        if started:
            print("Car is already stopped")
        else:
            stopped = True
            print("Car is stopped.")

    elif cmd == "help":
        print(
            """
start - to start the car
stop - to stop the car
quit - to exit the program """
        )
    elif cmd == "quit":
        break    
    else:
        print("Sorry, I don't understand that!")
